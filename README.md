# DỰ ĐOÁN LƯU LƯỢNG GIAO THÔNG DỰA VÀO GIẢI THUẬT GMM VÀ MACHINE LEARNING
## Tóm Tắt
Chúng tôi xin đề xuất một phương pháp dự đoán lưu lượng xe thông qua một mô hình Học máy. Dữ liệu đầu vào là video trích xuất từ camera sau khi được tiền xử lý qua các bước trừ nền bằng cách sử dụng mô hình GMM và lọc sẽ được dùng để đếm lưu lượng xe. Dữ liệu sau bước này sẽ được sử dụng để huấn luyện model và để dự đoán lưu lượng xe. 
<br/> Ở thời điểm hiện tại, chương trình đang làm việc với kết quả khả quan đối với các video đã được trích xuất sẵn. Trong tương lai, nhóm tác giả đề xuất việc tích hợp chương trình vào một hệ thống giám sát để có thể xử lý thời gian thực.

## Công nghệ sử dụng
- OpenCv
- Python
- PyQt
- Machine Learning SVM

## Lưu đồ thuật toán
- Update sau
 

## Kết quả

<img align="center" src="https://gitlab.com/nguyenduyquang06/opencv-traffic-predicting-gmm/raw/master/hinh/busy%202.png"/> <br/>
<img align="center" src="https://gitlab.com/nguyenduyquang06/opencv-traffic-predicting-gmm/raw/master/hinh/relax.png"/> <br/>

## Kết luận

Kết quả đạt được khả quan với độ chính xác cao trong nhiều điều kiện sáng khác nhau, tuy nhiên mã nguồn vẫn còn cồng kềnh nên chưa tối ưu được hiệu năng cao. Nhưng hoàn toàn có thể áp dụng cho những dự án thành phố thông minh trong tương lai gần.

## Author
- Tan Doan
- Quang Nguyen
- Phuc Luong
- Nguyen Nguyen
- Hoang Ly