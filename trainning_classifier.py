#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan  5 18:21:19 2019

@author: tandoan
"""

from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from skimage.feature import hog
from scipy.misc import imread
from sklearn.externals import joblib

import random as rand
import numpy as np 
import cv2
import glob
import time


traffic_highobj = pd.read_excel('traffic_highobj.xlsx', sheet_name='Sheet1')
Y = list(pd.read_excel('Y.xlsx')[0])
features = traffic_highobj[['Frames', 'total_cars']]
labels = Y
d_train, d_test, l_train, l_test = train_test_split(features, labels,test_size=0.3, random_state=0)

svc = SVC()
svc.fit(d_train, l_train)
accuracy = svc.score(d_test, l_test)

print ("Saving models...")
joblib.dump(svc, 'svc.pkl')
print("...Done")

print ("Loading models...")
svc = joblib.load('svc.pkl')
print("...Done")